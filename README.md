# PINE 

PINE
Pine is an aimbot powered by real-time object detection with neural networks.

This software can be tweaked to work smoothly in CS:GO, Fortnite, and Overwatch. Pine also has built-in support for Nvidia's CUDA toolkit and is optimized to achieve extremely high object-detection FPS. It is GPU accelerated, cross-platform, and blazingly fast.

Installation

Install OpenCV with Python3 bindings
Install any missing dependencies with Pip
Install the Nvidia CUDA toolkit if you have a compatible GPU
If you don't have an Nvidia GPU, Pine will try to use OpenCL instead of CUDA
Then run the program with Python3.
$ python3 pine.py

What games does it work with?
This release is currently optimized for CS:GO, but I plan adding more game configs in the future.